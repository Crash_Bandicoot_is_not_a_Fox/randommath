The generator is an implementation based on a static class that includes an object of the SHA2PRNGProvider class (provider of SHA256 unique hashes). 
The starting state of the hash provider is expressed as" nounce "(incremented with each call) and a certain" seed " set by the number of ticks of the current system timer value. 
An array of bytes for generating hashes is obtained by converting the sum of nounce and "seed" into an array of bytes, which ensures that the value of the array of bytes for taking the hash is unique for each initialization of the hash generator and each new hash receipt. The resulting hash itself (which is also an array of bytes)it is reduced to a UInt64 value and correlated to the maximum UInt64 value, which gives us a high degree of discretization of the spatial value in the diappazon [0 .. 1], while the SHA256 algorithm determines the uniform distribution of this value as a random variable.

Attach a link to the description of SHA-2:
https://en.wikipedia.org/wiki/SHA-2

The SHA256 object is taken from the System library.Security.Cryptography

A link to the documentation :
https://docs.microsoft.com/en-us/dotnet/api/system.security.cryptography?view=dotnet-plat-ext-3.1

Open source code :
https://github.com/microsoft/referencesource/tree/master/mscorlib/system/security/cryptography

An example of usage is given below: 

	public class RandomGenerator
	{
		private static readonly Lazy<RandomGenerator> Lazy = new Lazy<RandomGenerator>(() => new RandomGenerator());
		public static RandomGenerator Instance => Lazy.Value;

		/// <summary>
		/// Returns a nonnegative random integer that is less than the specified maximum.
		/// </summary>
		/// <param name="maxValue">The exclusive upper bound of the random number to be generated.
		/// maxValue must be greater than or equal to zero.</param>
		/// <returns>A 32-bit signed integer greater than or equal to zero, and less than maxValue;
		/// that is, the range of return values ordinarily includes zero but not maxValue.
		/// However, if maxValue equals zero, maxValue is returned.</returns>
		public int Next(int maxValue = int.MaxValue)
		{
			if (maxValue-1 < 0)
			{
				throw new ArgumentOutOfRangeException("minValue is lower than zero");
			}

			return Next(0, maxValue);
		}

		/// <summary>
		/// Returns a random integer that is within a specified range.
		/// </summary>
		/// <param name="minValue">The inclusive lower bound of the random number returned.</param>
		/// <param name="maxValue">The exclusive upper bound of the random number returned.
		/// maxValue must be greater than or equal to minValue.</param>
		/// <returns>A 32-bit signed integer greater than or equal to minValue and less than maxValue;
		/// that is, the range of return values includes minValue but not maxValue. If minValue
		/// equals maxValue, minValue is returned.</returns>
		public int Next(int minValue, int maxValue)
		{
			return RandomMath.GetRealRandomInt(minValue, maxValue-1);
		}

		/// <summary>
		/// Returns a random number between 0.0 and 1.0.
		/// </summary>
		/// <returns>
		/// A double-precision floating point number greater than or equal to 0.0, and less than 1.0.
		/// </returns>
		public double NextDouble()
		{
			return RandomMath.GetRandomValueReal();
		}

		public long NextLong(long minValue, long maxValue)
		{
			return RandomMath.GetRealRandomInt(minValue, maxValue-1);
		}

		public bool NextBool()
		{
			return RandomGenerator.Instance.Next(2) > 0;
		}
	}