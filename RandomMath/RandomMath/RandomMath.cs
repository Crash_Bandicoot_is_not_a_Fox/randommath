﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RandomMath
{
    /// <summary>
    /// Random mathematics class
    /// </summary>
    public static class RandomMath
    {
        static SHA2PRNGProvider innerSHA256 = new SHA2PRNGProvider();

        /// <summary>
        /// Returns the truly real random value betwean 0 and 1
        /// </summary>
        /// <returns>Случайное число</returns>
        public static double GetRandomValueReal()
        {
            return innerSHA256.Next();
        }

        class ObjAndDouble<T>
        {
            public T Obj;
            public double Dbl;
        }

        /// <summary>
        /// Collection random sorting extension
        /// </summary>
        /// <typeparam name="T"> Collection type </typeparam>
        /// <param name="ie"> Collection </param>
        /// <returns> Random sorted collection </returns>
        public static IEnumerable<T> RandomSort<T>(this IEnumerable<T> ie)
        {
            var ie_res = ie.Select(o => new ObjAndDouble<T> { Obj = o, Dbl = GetRandomValueReal() }).OrderBy(o => o.Dbl).Select(o => o.Obj);
            var count = ie_res.FirstOrDefault();
            return ie_res;
        }

        /// <summary>
        /// Returns the array of the truly real random values  betwean 0 and 1
        /// </summary>
        /// <param name="numberOfRandoms"> Array size </param>
        /// <returns></returns>
        public static double[] GetRandomValuesReal(int numberOfRandoms)
        {
            double[] res = new double[numberOfRandoms];
            for (int i = 0; i < numberOfRandoms; i++)
            {
                res[i] = GetRandomValueReal();
            }

            return res;
        }

        /// <summary>
        /// Returns the truly int random value betwean bottom and top values inclusive (advanced)
        /// </summary>
        /// <param name="bottom"> Bottom value </param>
        /// <param name="top"> Top value </param>
        /// <returns> Random integer value </returns>
        public static int GetRealRandomInt(int bottom, int top)
        {
            int result = 0;
            byte[] data = new byte[2];

            double val = innerSHA256.Next();
            double Number = val * (top + 1 - bottom);
            result = (int)(Number) + bottom;

            return result;
        }

        /// <summary>
        /// Returns the array of the truly int random values betwean bottom and top values inclusive
        /// </summary>
        /// <param name="bottom"> Bottom value </param>
        /// <param name="top"> Top value </param>
        /// <param name="num"> Array size </param>
        /// <returns> Array with the int random values </returns>
        public static int[] GetRealRandomInts(int bottom, int top, int num)
        {
            int[] result = new int[num];
            for (int i = 0; i < num; i++)
                result[i] = GetRealRandomInt(bottom, top);

            return result;
        }

        /// <summary>
        /// Returns the Sum value of a few random int values
        /// </summary>
        /// <param name="bottom"> Bottom value </param>
        /// <param name="top"> Top value </param>
        /// <param name="num"> Array size </param>
        /// <returns> Random int value </returns>
        public static long GetRealRandomIntsSum(int bottom, int top, int num)
        {
            return GetRealRandomInts(bottom, top, num).Sum();
        }



        /// <summary>
        /// Returns the truly long random value betwean bottom and top values inclusive
        /// </summary>
        /// <param name="bottom"> Bottom value </param>
        /// <param name="top"> Top value </param>
        /// <returns> Random long value </returns>
        public static long GetRealRandomInt(long bottom, long top)
        {
            long result = 0;
            byte[] data = new byte[2];

            double val = innerSHA256.Next();
            double Number = val * (top + 1 - bottom);
            result = (long)(Number) + bottom;

            return result;
        }

        /// <summary>
        /// Returns the int random value betwean bottom and top values inclusive
        /// </summary>
        /// <param name="bottom"> Bottom value </param>
        /// <param name="top"> Top value </param>
        /// <returns> Random integer value </returns>
        private static int GetIntRandom(int bottom, int top)
        {
            int result = 0;

            var rnd = new Random();
            result = rnd.Next(top - bottom) + bottom;

            return result;
        }

        /// <summary>
        /// Returns the truly int random value betwean bottom and top values inclusive
        /// </summary>
        /// <param name="bottom"> Bottom value </param>
        /// <param name="top"> Top value </param>
        /// <returns> Random integer value </returns>
        private static int GetIntRandomReal(int bottom, int top)
        {
            int result = 0;
            byte[] data = new byte[2];

            double val = innerSHA256.Next();
            double Number = val * (top + 1 - bottom);
            result = Convert.ToInt32(Math.Round(Number, 0)) + bottom;

            return result;
        }


    }
}
