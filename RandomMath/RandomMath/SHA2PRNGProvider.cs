﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace RandomMath
{
    public class SHA2PRNGProvider
    {
        private long seed = DateTime.UtcNow.Ticks;
        private long nounce = 0;
        private SHA256 sha256 = SHA256.Create();

        private readonly object sync = new object();

        public SHA2PRNGProvider()
        {
            string dummy = string.Empty;
            seed += dummy.GetHashCode();
        }

        public double Next()
        {
            lock (sync)
            {
                double val = 0.0;

                var bytes = BitConverter.GetBytes(seed + nounce);
                byte[] hashValue = sha256.ComputeHash(bytes);
                UInt64 rndUInt = BitConverter.ToUInt64(hashValue, 0);

                val = Convert.ToDouble(rndUInt >> 11) * 1.1102230246251565E-16;

                nounce++;

                return val;
            }
        }
    }
}
