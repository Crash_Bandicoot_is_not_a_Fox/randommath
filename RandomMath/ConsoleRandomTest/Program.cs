﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleRandomTest
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> results = new List<int>();

            for (int i = 0; i < 100000; i++)
            {
                results.Add(RandomGenerator.Instance.Next(10));
            }

            foreach (var el in results.Distinct())
            {
                Console.WriteLine($"{el}, percent {results.Count(e => e == el) / (decimal)results.Count() }");
            }



            Console.WriteLine("Hello World!");
        }
    }
}
